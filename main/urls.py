from django.urls import path
from django.conf.urls import url


from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('data/',views.data_func, name='data'),
    path('signup/', views.signup, name='signup'),
    # path('register/', views.register, name='register'),

    
]
