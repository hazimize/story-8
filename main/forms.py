from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class SignUpForm(UserCreationForm):
    birth_date = forms.DateField(help_text='Required. Format: YYYY-MM-DD')

    class Meta:
        model = User
        fields = ('username', 'birth_date', 'password1', 'password2')


    def save(self, commit=True):
        user = super (SignUpForm , self ).save(commit=False)
        user.username = self.cleaned_data ['username']
        user.birth_date = self.cleaned_data ['birth_date']
        user.password1 = self.cleaned_data ['password1']
        user.password2 = self.cleaned_data ['password2']
        user.is_staff = True

        if commit :
            user.save()

        return user
# from django import forms
# from django.contrib.auth.forms import UserCreationForm
# from django.contrib.auth.models import User


# # Create your forms here.

# class NewUserForm(UserCreationForm):
# 	email = forms.EmailField(required=True)

# 	class Meta:
# 		model = User
# 		fields = ("username", "email", "password1", "password2")

# 	def save(self, commit=True):
# 		user = super(NewUserForm, self).save(commit=False)
# 		user.email = self.cleaned_data['email']
# 		if commit:
# 			user.save()
# 		return user